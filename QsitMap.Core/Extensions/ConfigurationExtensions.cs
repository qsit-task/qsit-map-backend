﻿using System;
using Microsoft.Extensions.Configuration;

namespace QsitMap
{
    public static class ConfigurationExtensions
    {
        public static string GetAppURL(this IConfiguration Configuration)
        {
            var AppURL = Configuration.GetValue("AppURL", "");
            return AppURL;
        }

        public static string GetStoragePath(this IConfiguration Configuration)
        {
            var StoragePath = Configuration.GetValue("StoragePath", "");
            return StoragePath;
        }
    }
}
