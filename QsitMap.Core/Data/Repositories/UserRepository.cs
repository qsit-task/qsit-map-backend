﻿using Microsoft.AspNetCore.Identity;
using MZCore.Patterns.Repositroy;
using QsitMap.Core.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QsitMap.Core.Data.Repositories
{
    public class UserRepository : EntityRepository<ApplicationDbContext, User, long>, IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserRepository(ApplicationDbContext context, UserManager<User> userManager,
            SignInManager<User> signInManager) : base(context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<User> FindByUsernamePasswordAsync(string username, string password)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(username);
                var valid = await _signInManager.UserManager.CheckPasswordAsync(user, password);
                if (valid)
                {
                    return user;
                }
                else 
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }
    }
}
