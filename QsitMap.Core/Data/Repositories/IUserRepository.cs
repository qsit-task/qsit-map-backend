﻿using MZCore.Patterns.Repositroy;
using QsitMap.Core.Data.Entities;
using System.Threading.Tasks;

namespace QsitMap.Core.Data.Repositories
{
    public interface IUserRepository : IRepository<User, long>
    {
        public Task<User> FindByUsernamePasswordAsync(string email, string password);
    }
}
