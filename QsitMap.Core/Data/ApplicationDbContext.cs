﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using QsitMap.Core.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using QsitMap.Core.Data.Seeding;

namespace QsitMap.Core.Data
{
    public class ApplicationDbContext : IdentityDbContext<User, IdentityRole<long>, long>
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppVersion> AppVersion { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<MapConfiguration> MapConfigurations { get; set; }
        public virtual DbSet<MapType> MapTypes { get; set; }
        public virtual DbSet<MapSubType> MapSubTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // modelBuilder.SeedRoles();
            // modelBuilder.SeedUsers();
        }
    }
}
