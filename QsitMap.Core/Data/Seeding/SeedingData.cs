﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using QsitMap.Core.Data.Entities;

namespace QsitMap.Core.Data.Seeding
{
    internal static class SeedingData
    {
        public static void SeedRoles(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IdentityRole<long>>().HasData(
                new IdentityRole<long> { Id = 1, ConcurrencyStamp = "167860ad-4ade-4725-92b9-d8b57815b919", Name = "Administrator", NormalizedName = "ADMINISTRATOR".ToUpper() },
                new IdentityRole<long> { Id = 2, ConcurrencyStamp = "5bb79958-bc8b-4575-8a44-25460b1e2936", Name = "Customer", NormalizedName = "Customer".ToUpper() }
            );
        }

        public static void SeedUsers(this ModelBuilder modelBuilder)
        {
            //a hasher to hash the password before seeding the user to the db
            var hasher = new PasswordHasher<User>();
            var SecurityStamp = Guid.NewGuid().ToString();
            var password = hasher.HashPassword(null, "#$3_!4&7F?Zb"); // AQAAAAEAACcQAAAAEA0dFJ7ebrmyrqdtL1eV5ttvgEi+6KSfBo4SVQIuvwsiqzcc18PrmHJ+sg8beske+w==

            //Seeding the User to AspNetUsers table
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1, // primary key
                    UserName = "Admin",
                    NormalizedUserName = "Admin".ToUpper(),
                    Email = "Admin",
                    NormalizedEmail = "Admin",
                    ConcurrencyStamp = "d6e2c1f1-3a7b-4cf0-aea5-05deb4f12df7",
                    SecurityStamp = "27c0b512-9f7e-4ce7-bcff-6563379cbe20",
                    EmailConfirmed = true,
                    PhoneNumber = "01000000001",
                    PasswordHash = "AQAAAAEAACcQAAAAEBu3ShA1B6T9d8Hu1/JYIVWNOqOZ2vy2/RIj3CC5g1gosnRRBk/aPLrP0YI9EowIsQ==" // = "q?$A!P_D5eT&D2BB"
                },
                new User
                {
                    Id = 2, // primary key
                    UserName = $"tester",
                    NormalizedUserName = "tester".ToUpper(),
                    Email = $"tester@test.com",
                    NormalizedEmail = "tester@test.com".ToUpper(),
                    ConcurrencyStamp = "7ae9942e-353c-4d43-a59f-2c516e8e2cb5",
                    SecurityStamp = "09aeb77e-bf2a-4dde-a9a2-1a02537f4702",
                    EmailConfirmed = true,
                    PhoneNumber = "01000000001",
                    PasswordHash = "AQAAAAEAACcQAAAAEPKEb9qQEkCFTrrwp5d7W5KccnxWDI6g/MMhva5o2boop+zYynmuHjperLZ7i4Bmrg==" // => 123456
                }
            );

            modelBuilder.Entity<IdentityUserRole<long>>().HasData(
                new IdentityUserRole<long>
                {
                    RoleId = 1, // Administrator
                    UserId = 1
                },
                new IdentityUserRole<long>
                {
                    RoleId = 2, // Customer
                    UserId = 2
                }
            );

            modelBuilder.Entity<MapType>().HasData(
                new MapType
                {
                    Id = 1,
                    Name = "Features"
                },
                new MapType
                {
                    Id = 2,
                    Name = "Basemap"
                }
            );

            modelBuilder.Entity<MapSubType>().HasData(
                new MapSubType
                {
                    Id = 1,
                    Name = "Dynamic",
                    MapTypeId = 1
                },
                new MapSubType
                {
                    Id = 2,
                    Name = "Cached",
                    MapTypeId = 1
                },

                new MapSubType
                {
                    Id = 3,
                    Name = "Imagery",
                    MapTypeId = 2
                },
                new MapSubType
                {
                    Id = 4,
                    Name = "Topographic",
                    MapTypeId = 2
                }
            );
        }
    }
}
