﻿using System;
namespace QsitMap.Core.Data.Entities
{
    public class Notification : BaseEntity<int>
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }
    }
}
