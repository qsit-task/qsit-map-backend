﻿using System;
namespace QsitMap.Core.Data.Entities
{
    public class MapSubType : BaseEntity<byte>
    {
        public string Name { get; set; }

        public byte MapTypeId { get; set; }
    }
}

