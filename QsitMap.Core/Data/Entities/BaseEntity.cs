﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Infrastructure;
using MZCore.Patterns.Repositroy;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace QsitMap.Core.Data.Entities
{
    /// <summary>
    /// Represents the base class for entities
    /// </summary>
    public abstract partial class BaseEntity<TKey> : IEntity<TKey>
        where TKey : struct, IComparable, IComparable<TKey>, IConvertible, IEquatable<TKey>, IFormattable
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [SwaggerSchema("The entity identifier", ReadOnly = true)]
        public virtual TKey Id { get; set; }

        [NotNull]
        [SwaggerSchema(ReadOnly = true)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [AllowNull]
        [SwaggerSchema(ReadOnly = true)]
        public DateTime? UpdatedAt { get; set; }

        [AllowNull]
        [SwaggerSchema(ReadOnly = true)]
        public DateTime? DeletedAt { get; set; }

        [AllowNull]
        [SwaggerSchema(ReadOnly = true)]
        public long? CreatedBy { get; set; }

        [AllowNull]
        [SwaggerSchema(ReadOnly = true)]
        public long? UpdatedBy { get; set; }

        protected ILazyLoader LazyLoader { get; set; }
    }
}
