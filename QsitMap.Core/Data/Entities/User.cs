﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;
using MZCore.Patterns.Repositroy;

namespace QsitMap.Core.Data.Entities
{
    public class User : IdentityUser<long>, IEntity<long>
    {
        [JsonIgnore]
        public ICollection<RefreshToken> RefreshTokens { get; set; }

        public long? CreatedBy { get; set; }
        public long? UpdatedBy { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
