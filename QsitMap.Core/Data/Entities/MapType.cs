﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace QsitMap.Core.Data.Entities
{
    public class MapType : BaseEntity<byte>
    {
        public string Name { get; set; }

        private ICollection<MapSubType> _MapSubTypes;

        public virtual ICollection<MapSubType> MapSubTypes
        {
            get => LazyLoader.Load(this, ref _MapSubTypes);
            set => _MapSubTypes = value;
        }
    }
}

