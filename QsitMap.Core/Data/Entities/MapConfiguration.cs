﻿using System;
using System.ComponentModel.DataAnnotations;
using Swashbuckle.AspNetCore.Annotations;

namespace QsitMap.Core.Data.Entities
{
    public class MapConfiguration : BaseEntity<int>
    {
        // Map Settings
        [Required]
        [Range(0, 99.1000, ErrorMessage = "Zero is Not allowed, the value must be between 0.01 to 99.999")]
        [RegularExpression(@"^\d+(\.\d{1,3})?$", ErrorMessage = "The maximum allowed fractions is 3 decimals")]
        public float? ClusterRadius { get; set; }

        [Required]
        public bool? GeoFencing { get; set; }

        // Duplication Event Configuration
        [Required]
        [Range(0, 99.1000, ErrorMessage = "Zero is Not allowed, the value must be between 0.01 to 99.999")]
        [RegularExpression(@"^\d+(\.\d{1,3})?$", ErrorMessage = "The maximum allowed fractions is 3 decimals")]
        public float? TimeBuffer { get; set; }

        [Required]
        [Range(0, 99.1000, ErrorMessage = "Zero is Not allowed, the value must be between 0.01 to 99.999")]
        [RegularExpression(@"^\d+(\.\d{1,3})?$", ErrorMessage = "The maximum allowed fractions is 3 decimals")]
        public float? LocationBuffer { get; set; }

        // End Event Duration
        [Required]
        [Range(0, 99.1000, ErrorMessage = "Zero is Not allowed, the value must be between 0.01 to 99.999")]
        [RegularExpression(@"^\d+(\.\d{1,3})?$", ErrorMessage = "The maximum allowed fractions is 3 decimals")]
        public float? Duration { get; set; }

        [Required]
        public byte? MapTypeId { get; set; }

        [Required]
        public byte? MapSubTypeId { get; set; }
    }
}

