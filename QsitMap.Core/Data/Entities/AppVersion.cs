using System.ComponentModel.DataAnnotations;
using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace QsitMap.Core.Data.Entities
{
    public class AppVersion : BaseEntity<byte>
    {
        [Required]
        [JsonPropertyName("versionCode")]
        [JsonProperty(PropertyName = "versionCode")]
        public int VersionCode { get; set; }

        [Required]
        [JsonPropertyName("updateRequired")]
        [JsonProperty(PropertyName = "updateRequired")]
        public bool UpdateRequired { get; set; }
    }
}