﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using QsitMap.Core.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var mySqlConnectionString = builder.Configuration.GetConnectionString("DefaultConnection");

var migrationsAssembly = typeof(Program).GetTypeInfo().Assembly.GetName().Name;

builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(mySqlConnectionString, b => b.MigrationsAssembly(migrationsAssembly));
});

var app = builder.Build();

app.Run();

