#!/usr/bin/env bash
echo "$CI_GOOGLE_KEY" > google.key.json
echo "$CI_REGISTRY_SECRET" > registry-gitlab-secret.yml
./build-deployment-file.sh
./build-deployment-env-file.sh
gcloud auth activate-service-account --key-file google.key.json
gcloud config set container/use_client_certificate False
getClusters=$(gcloud container --project $CI_GOOGLE_PROJECT_NAME clusters list)
echo "get clusters result: $getClusters"
if [[ $getClusters == *"$CI_GOOGLE_CLUSTER_NAME"* ]]; then
    echo "$CI_GOOGLE_CLUSTER_NAME exists and no need to create it!"
else
    echo "$CI_GOOGLE_CLUSTER_NAME doesn't exist and is being created!"
    gcloud container --project $CI_GOOGLE_PROJECT_NAME clusters create $CI_GOOGLE_CLUSTER_NAME --zone $CI_GOOGLE_CLUSTER_ZONE --no-enable-basic-auth --cluster-version "1.22.8-gke.202" --release-channel "regular" --machine-type "e2-medium" --image-type "COS_CONTAINERD" --disk-type "pd-ssd" --disk-size "40" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node "110" --num-nodes "1" --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/qsit-map/global/networks/default" --subnetwork "projects/qsit-map/regions/us-central1/subnetworks/default" --no-enable-intra-node-visibility --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --node-locations $CI_GOOGLE_CLUSTER_ZONE
fi
gcloud container clusters get-credentials $CI_GOOGLE_CLUSTER_NAME --zone $CI_GOOGLE_CLUSTER_ZONE --project $CI_GOOGLE_PROJECT_NAME
kubectl apply -f namespaces-deployment.yml
kubectl apply -f registry-gitlab-secret.yml --namespace=$CI_DEPLOYMENT_NAMESPACE
kubectl apply -f deployment-env.yml --namespace=$CI_DEPLOYMENT_NAMESPACE
kubectl apply -f deployment.yml --namespace=$CI_DEPLOYMENT_NAMESPACE