﻿using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using QsitMap.Core.Data.Entities;

namespace QsitMap.API.Models
{
    public class AuthenticateResponse
    {
        [JsonPropertyName("status")]
        [JsonProperty(PropertyName = "status")]
        public bool Status { get; set; }

        [JsonPropertyName("code")]
        [JsonProperty(PropertyName = "code")]
        public int Code { get; set; }

        [JsonPropertyName("message")]
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        [JsonPropertyName("access_token")]
        [JsonProperty(PropertyName = "access_token")]
        public string JwtToken { get; set; }

        [JsonPropertyName("refresh_token")]
        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }

        [JsonPropertyName("data")]
        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }

        public AuthenticateResponse(string jwtToken, string refreshToken, object data = null)
        {
            JwtToken = jwtToken;
            RefreshToken = refreshToken;
            Message = "Successful login";
            Code = 200;
            Status = true;
            Data = new
            {
                accessToken = jwtToken
            };
        }
    }
}
