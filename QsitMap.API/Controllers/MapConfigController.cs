﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MZCore.Patterns.Generices;
using MZCore.Patterns.Repositroy;
using QsitMap.Core.Data.Entities;

namespace QsitMap.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MapConfigController : GenericAPIRepoController<IRepository<MapConfiguration, int>, MapConfiguration, int>
    {
        public MapConfigController(IRepository<MapConfiguration, int> repository)
            : base(repository)
        {
        }
    }
}

