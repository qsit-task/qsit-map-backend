﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QsitMap.API.Models;
using QsitMap.Core.Data;
using QsitMap.Core.Data.Repositories;
using QsitMap.Core.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using QsitMap.API.Services;

namespace QsitMap.API.Controllers
{
    public class AuthController : BaseAPIController<ApplicationDbContext>
    {
        private readonly IUserService _userService;

        public AuthController(ApplicationDbContext context, IUserService userService)
            : base(context)
        {
            _userService = userService;
        }

        // POST: auth/login
        [AllowAnonymous]
        [HttpPost("swagger/login")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult<AuthenticateResponse>> PostUsersLoginForSwagger([FromForm] AuthenticateRequest loginModel)
        {
            return await _userService.Authenticate(loginModel, ipAddress());
        }

        // POST: auth/login
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<AuthenticateResponse>> PostUsersLogin(AuthenticateRequest loginModel)
        {
            return await _userService.Authenticate(loginModel, ipAddress());
        }

        // POST: auth/login
        [AllowAnonymous]
        [HttpPost("refresh_token")]
        public async Task<ActionResult<AuthenticateResponse>> PostRefreshToken(string refreshToken)
        {
            return await _userService.RefreshToken(refreshToken, ipAddress());
        }

        private string ipAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }
    }
}
