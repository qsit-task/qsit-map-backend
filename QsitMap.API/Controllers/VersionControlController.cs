using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MZCore.Helpers;
using QsitMap.API.Models;
using QsitMap.Core.Data;
using QsitMap.Core.Data.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace QsitMap.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class VersionControlController : BaseAPIController<ApplicationDbContext>
    {
        private readonly IConfiguration Configuration;

        public VersionControlController(ApplicationDbContext context, IConfiguration configuration) : base(context)
        {
            Configuration = configuration;
        }

        // GET: api/Specialities
        [HttpGet("getAppVersion")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Notification>>> GetAppVersion()
        {
            var appVersion = await _dbContext.AppVersion.Where(e => e.Id == 1).FirstOrDefaultAsync();
            return StatusCode(StatusCodes.Status200OK, new
            {
                status = true,
                code = 200,
                message = "Version Controll",
                data = appVersion
            });
        }

        [HttpPost("updateAppVersion")]
        public async Task<ActionResult<IEnumerable<Notification>>> UpdateAppVersion(AppVersion appVersion)
        {
            _dbContext.AppVersion.Update(appVersion);
            await _dbContext.SaveChangesAsync();
            return StatusCode(StatusCodes.Status200OK, new
            {
                status = true,
                code = 200,
                message = "Version Control Updated",
                data = appVersion
            });
        }

        [AllowAnonymous]
        [HttpGet("/AppDownload")]
        public IActionResult AppDownload()
        {
            return Redirect(Configuration.GetAppURL() + "/AppDownload");
        }
    }
}
