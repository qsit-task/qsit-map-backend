﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using QsitMap.API.Models;
using QsitMap.Core.Data;
using QsitMap.Core.Data.Entities;
using QsitMap.Core.Data.Repositories;

namespace QsitMap.API.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository UserRepository;
        private readonly IConfiguration Configuration;
        private readonly ApplicationDbContext _dbContext;

        public UserService(ApplicationDbContext context, IUserRepository userRepository, IConfiguration configuration)
        {
            UserRepository = userRepository;
            Configuration = configuration;
            _dbContext = context;
        }

        public async Task<AuthenticateResponse> Authenticate(AuthenticateRequest model, string ipAddress)
        {
            var user = await UserRepository.FindByUsernamePasswordAsync(model.Username, model.Password);
            if (user == null)
            {
                throw new UnauthorizedAccessException("Incorrect username or password, please try again!");
            }
            else
            {
                if (!user.EmailConfirmed)
                {
                    throw new UnauthorizedAccessException("Please check your email first to confirm your account, then try again!");
                }
                // authentication successful so generate jwt and refresh tokens
                var data = new { };
                var userRole = await _dbContext.UserRoles.Where(e => e.UserId == user.Id).FirstOrDefaultAsync();
                var role = await _dbContext.Roles.Where(e => e.Id == userRole.RoleId).FirstOrDefaultAsync();
                var jwtToken = generateJwtToken(user, role.Name);
                var refreshToken = generateRefreshToken(ipAddress);

                // save refresh token
                user.RefreshTokens.Add(refreshToken);
                _dbContext.Update(user);
                _dbContext.SaveChanges();

                return new AuthenticateResponse(
                    jwtToken,
                    refreshToken.Token);
            }
        }

        public async Task<AuthenticateResponse> RefreshToken(string token, string ipAddress)
        {
            var user = _dbContext.Users.SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

            // return null if no user found with token
            if (user == null) throw new UnauthorizedAccessException("Invalid token!");

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            // return null if token is no longer active
            if (!refreshToken.IsActive) throw new UnauthorizedAccessException("Invalid token!");

            // replace old refresh token with a new one and save
            var newRefreshToken = generateRefreshToken(ipAddress);
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            refreshToken.ReplacedByToken = newRefreshToken.Token;
            user.RefreshTokens.Add(newRefreshToken);
            _dbContext.Update(user);
            _dbContext.SaveChanges();

            var data = new { };
            var userRole = await _dbContext.UserRoles.Where(e => e.UserId == user.Id).FirstOrDefaultAsync();
            var role = await _dbContext.Roles.Where(e => e.Id == userRole.RoleId).FirstOrDefaultAsync();
            // generate new jwt
            var jwtToken = generateJwtToken(user, role.Name);
            return new AuthenticateResponse(
                    jwtToken,
                    newRefreshToken.Token);
        }

        public bool RevokeToken(string token, string ipAddress)
        {
            var user = _dbContext.Users.SingleOrDefault(u => u.RefreshTokens.Any(t => t.Token == token));

            // return false if no user found with token
            if (user == null) return false;

            var refreshToken = user.RefreshTokens.Single(x => x.Token == token);

            // return false if token is not active
            if (!refreshToken.IsActive) return false;

            // revoke token and save
            refreshToken.Revoked = DateTime.UtcNow;
            refreshToken.RevokedByIp = ipAddress;
            _dbContext.Update(user);
            _dbContext.SaveChanges();

            return true;
        }

        // helper methods

        private string generateJwtToken(User user, string roleName)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var authSigningKey = Encoding.ASCII.GetBytes(Configuration["JWT:Secret"]);
            var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, roleName),
                    new Claim("UserID", user.Id.ToString()),
                    new Claim("UserRole", roleName)
                };
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = Configuration["JWT:ValidIssuer"],
                Audience = Configuration["JWT:ValidAudience"],
                Subject = new ClaimsIdentity(authClaims),
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(authSigningKey), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private RefreshToken generateRefreshToken(string ipAddress)
        {
            var randomBytes = RandomNumberGenerator.GetBytes(64);
            return new RefreshToken
            {
                Token = Convert.ToBase64String(randomBytes),
                Expires = DateTime.UtcNow.AddDays(365 * 5),
                Created = DateTime.UtcNow,
                CreatedByIp = ipAddress
            };
        }
    }
}
